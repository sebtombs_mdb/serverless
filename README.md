# serverless

Repository for public serverless framework services

In here are an assortment of services, initially only targeted at AWS, using the [Serverless Framework](https://serverless.com)

They are all open for public use.